# OpenML dataset: Movies-on-Netflix,-Prime-Video,-Hulu-and-Disney

https://www.openml.org/d/43309

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The dataset is an amalgamation of: 
data that was scraped, which comprised a comprehensive list of movies available on various streaming platforms
IMDb dataset

Inspiration
Which streaming platform(s) can I find this movie on  
Average IMDb rating of movies produced in a country
Target age group movies vs the streaming application they can be found on
The year during which a movie was produced and the streaming platform they can be found on
Analysis of the popularity of a movie vs directors

Data visualization of the above can be found on: https:public.tableau.comprofileruchi.bhatia#vizhomeMoviesavailableonstreamingplatformsMoviesavailableonstreamingapplications

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43309) of an [OpenML dataset](https://www.openml.org/d/43309). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43309/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43309/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43309/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

